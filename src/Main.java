public class Main {
    public static void main(String[] args) throws Exception {

        // task b
        int[] initialNumericData = new int[] {4, 5, 1, 4, 3, 9, 7, 4, 1, 2, 0, -4};

        NumericArrayWorker numericArrayWorker = new NumericArrayWorker(initialNumericData);

        System.out.println(numericArrayWorker.getFrequency(0));

        // task a
        String initialSentenceData = "Саша  убил и Машу, и Дашу, и Свету, а Олега так и вовсе не пожалел: изнасиловал целых (!) 2 раза подряд";

        SentenceAnalyzer sentenceAnalyzer = new SentenceAnalyzer(initialSentenceData);
        // task a 1
        for (String currentWord :
                sentenceAnalyzer.getWordsSameLengthAsLast()) {
            System.out.print(currentWord + " ");
        }

        System.out.println();

        // task a 2
        int n = 4;

        for (String currentWord :
                sentenceAnalyzer.getFirstWords(n)) {
            System.out.print(currentWord + " ");
        }

        System.out.println();

        // task c
        for (char currentLetter :
                sentenceAnalyzer.getLeastFrequentLetters(true)) {
            System.out.print(currentLetter + " ");
        }

        System.out.println();

        // task d
        System.out.println(sentenceAnalyzer.replaceCommasWithAnd());
    }
}